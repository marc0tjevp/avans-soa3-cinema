package domain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.time.DayOfWeek;
import java.io.FileWriter;

public class Order {
    private int orderNr;
    private boolean isStudentOrder;
    private ArrayList<MovieTicket> tickets;

    public Order(int orderNr, boolean isStudentOrder) {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;
        tickets = new ArrayList<>();
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket) {
        tickets.add(ticket);
    }

    public double calculatePrice() {
        double totalPrice = 0;
        boolean isStudent = this.isStudentOrder;
        boolean isGroup = this.tickets.size() > 5;
        boolean flipNext = false;

        for (MovieTicket ticket : tickets) {

            // Student Logic
            if (isStudent) {
                if (!flipNext) {
                    if (ticket.isPremiumTicket()) totalPrice += (ticket.getPrice() + 2);
                    else totalPrice += ticket.getPrice();
                    flipNext = true;
                } else {
                    flipNext = false;
                }
            }

            // Dow Logic
            else if (isWeekend(ticket)) {
                if (ticket.isPremiumTicket()) totalPrice += (ticket.getPrice() + 3);
                else totalPrice += ticket.getPrice();
            } else {
                if (!flipNext) {
                    if (ticket.isPremiumTicket()) totalPrice += (ticket.getPrice() + 3);
                    else totalPrice += ticket.getPrice();
                    flipNext = true;
                } else {
                    flipNext = false;
                }
            }
        }

        // More than 5 people and no students.
        if (!isStudent && isGroup) totalPrice = totalPrice * 0.9;

        return totalPrice;
    }

    private boolean isWeekend(MovieTicket ticket) {
        DayOfWeek dayOfWeek = ticket.getMovieScreening().getDateAndTime().getDayOfWeek();
        return dayOfWeek == DayOfWeek.FRIDAY || dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY;
    }

    public void export(TicketExportFormat exportFormat) throws IOException {
        ArrayList<String> writeLst = new ArrayList<>();

        for (MovieTicket ticket : this.tickets) {
            writeLst.add(ticket.toString());
        }

        if (exportFormat == TicketExportFormat.PLAINTEXT) {
            try (FileWriter writer = new FileWriter("Order_ " + this.orderNr + ".txt")) {
                for (String str : writeLst) {
                    writer.write(str + System.lineSeparator());
                }
            }
        }

        if (exportFormat == TicketExportFormat.JSON){
            try (Writer writer = new FileWriter("Order_ " + this.orderNr + ".json")) {
                Gson gson = new GsonBuilder().create();
                gson.toJson(writeLst, writer);
            }
        }
    }

    @Override
    public String toString() {
        return "domain.Order{" +
                "orderNr=" + orderNr +
                ", isStudentOrder=" + isStudentOrder +
                ", tickets=" + tickets +
                '}';
    }
}
