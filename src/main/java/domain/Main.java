package domain;

import definition.OrderStateDefinition;

public class Main {

    public static void main(String[] args) {
        Order order = new Order(1, false);
        OrderStateDefinition context = new OrderStateDefinition(order);

        context.submit();
    }
}
