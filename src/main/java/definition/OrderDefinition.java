package definition;

import state.OrderState;

public interface OrderDefinition {
    /**
     * Sets the current state
     * @param state
     */
    void setState(OrderState state);

    /**
     * Submit
     */
    void submit();

    /**
     * Provision
     */
    void provision();

    /**
     * Pay
     */
    void pay();

    /**
     * Cancel
     */
    void cancel();
}
