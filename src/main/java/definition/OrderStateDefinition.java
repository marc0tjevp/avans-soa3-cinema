package definition;

import domain.Order;
import state.OrderCreatedState;
import state.OrderState;

public class OrderStateDefinition implements OrderDefinition {
    private Order order;
    private OrderState state;

    public OrderStateDefinition(Order order) {
        this.order = order;
        this.state = new OrderCreatedState(); // Initial state is created!
    }

    @Override
    public void setState(OrderState state) {
        this.state = state;
    }

    @Override
    public void submit() {
        this.state.submit(this, this.order);
    }

    @Override
    public void provision() {
        this.state.provision(this, this.order);
    }

    @Override
    public void pay() {
        this.state.pay(this, this.order);
    }

    @Override
    public void cancel() {
        this.state.cancel(this, this.order);
    }

    @Override
    public String toString() {
        return "OrderStateDefinition{" +
                "order=" + order +
                ", state=" + state +
                '}';
    }
}
