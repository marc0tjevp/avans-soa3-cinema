package definition;

import domain.MovieTicket;

public interface MovieTicketExportDefinition {
    /**
     * Exports a MovieTicket
     * @param ticket
     */
    void export(MovieTicket ticket);
}
