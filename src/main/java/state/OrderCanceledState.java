package state;

import definition.OrderDefinition;
import domain.Order;

public class OrderCanceledState implements OrderState {

    @Override
    public void submit(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to create/edit a canceled order");
    }

    @Override
    public void provision(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to provision a canceled order");
    }

    @Override
    public void pay(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to pay for a canceled order");
    }

    @Override
    public void cancel(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to cancel a canceled order");
    }
}
