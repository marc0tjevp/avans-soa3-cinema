package state;

import definition.OrderDefinition;
import domain.Order;

public class OrderProvisionalState implements OrderState {
    @Override
    public void submit(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to edit/create a provisional order");
    }

    @Override
    public void provision(OrderDefinition def, Order order) {
        def.setState(new OrderCanceledState());
    }

    @Override
    public void pay(OrderDefinition def, Order order) {
        def.setState(new OrderCompletedState());
    }

    @Override
    public void cancel(OrderDefinition def, Order order) {
        def.setState(new OrderCanceledState());
    }
}
