package state;

import domain.Order;

public interface OrderState {

    /**
     * Submit
     *
     * @param def
     * @param order
     */
    void submit(definition.OrderDefinition def, Order order);

    /**
     * Provision
     *
     * @param def
     * @param order
     */
    void provision(definition.OrderDefinition def, Order order);

    /**
     * Pay
     *
     * @param def
     * @param order
     */
    void pay(definition.OrderDefinition def, Order order);

    /**
     * Cancel
     *
     * @param def
     * @param order
     */
    void cancel(definition.OrderDefinition def, Order order);
}
