package state;

import definition.OrderDefinition;
import domain.Order;

public class OrderCreatedState implements OrderState {
    @Override
    public void submit(OrderDefinition def, Order order) {
        def.setState(new OrderReservedState());
    }

    @Override
    public void provision(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to provision a created order");
    }

    @Override
    public void pay(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to pay for a created order, reserve first");
    }

    @Override
    public void cancel(OrderDefinition def, Order order) {
        def.setState(new OrderCanceledState());
    }
}
