package state;

import definition.OrderDefinition;
import domain.Order;

public class OrderCompletedState implements OrderState {

    @Override
    public void submit(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to edit/create a completed order");
    }

    @Override
    public void provision(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to provision a completed order");
    }

    @Override
    public void pay(OrderDefinition def, Order order) {
        throw new UnsupportedOperationException("Unable to pay for completed order");
    }

    @Override
    public void cancel(OrderDefinition def, Order order) {
        def.setState(new OrderCanceledState());
    }
}
