package state;

import definition.OrderDefinition;
import domain.Order;

public class OrderReservedState implements OrderState {
    @Override
    public void submit(OrderDefinition def, Order order) {
        def.setState(new OrderReservedState());
    }

    @Override
    public void provision(OrderDefinition def, Order order) {
        def.setState(new OrderProvisionalState());
    }

    @Override
    public void pay(OrderDefinition def, Order order) {
        def.setState(new OrderCompletedState());
    }

    @Override
    public void cancel(OrderDefinition def, Order order) {
        def.setState(new OrderCanceledState());
    }
}
